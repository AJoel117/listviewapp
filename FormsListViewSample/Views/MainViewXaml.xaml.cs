﻿using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace FormsListViewSample
{
	
	public partial class MainViewXaml : ContentPage
	{
		public ObservableCollection<VeggieViewModel> veggies { get; set; }
		public MainViewXaml ()
		{
            InitializeComponent();

            veggies = new ObservableCollection<VeggieViewModel> ();
            lstView.ItemsSource = veggies;
        }
    }
}

