﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

//Joel Antony, Christopher Still, Josh Kemp
//Group: Runtime Terror
//Tutorial and sources used: https://docs.microsoft.com/en-us/dotnet/api/xamarin.forms.listview?view=xamarin-forms
namespace FormsListViewSample
{
	public class MainViewCode : ContentPage
	{
		public ObservableCollection<VeggieViewModel> veggies { get; set; }
		public MainViewCode()
		{
			veggies = new ObservableCollection<VeggieViewModel>();
			ListView lstView = new ListView();
			lstView.RowHeight = 60;
			this.Title = "ListView (Group:Runtime Terror)";
			lstView.ItemTemplate = new DataTemplate(typeof(CustomVeggieCell));
			veggies.Add(new VeggieViewModel { Name = "Tomato", Type = "Fruit", Image = "tomato.jpeg" });
			veggies.Add(new VeggieViewModel { Name = "Romaine Lettuce", Type = "Vegetable", Image = "lettuce.png" });
			veggies.Add(new VeggieViewModel { Name = "Zucchini", Type = "Vegetable", Image = "zucchini.png" });
			veggies.Add(new VeggieViewModel { Name = "Potato", Type = "Vegetable", Image = "potato.jpeg" });
			veggies.Add(new VeggieViewModel { Name = "Apple", Type = "Fruit", Image = "apple.jpg" });
			veggies.Add(new VeggieViewModel { Name = "Carrot", Type = "Vegetable", Image = "carrot.png" });
			veggies.Add(new VeggieViewModel { Name = "Broccoli", Type = "Vegetable", Image = "broccoli.png" });
			veggies.Add(new VeggieViewModel { Name = "Strawberry", Type = "Fruit", Image = "strawberry.png" });
			veggies.Add(new VeggieViewModel { Name = "Orange", Type = "Fruit", Image = "orange.png" });
			veggies.Add(new VeggieViewModel { Name = "Spinach", Type = "Vegetable", Image = "spinach.jpg" });
			veggies.Add(new VeggieViewModel { Name = "Onion", Type = "Vegetable", Image = "onion.jpg" });


			lstView.ItemsSource = veggies;
			Content = lstView;
		}

		public class CustomVeggieCell : ViewCell
		{
			public CustomVeggieCell()
			{
				//instantiate each of our views
				var image = new Image();
				var nameLabel = new Label();
				var typeLabel = new Label();
				var verticaLayout = new StackLayout();
				var horizontalLayout = new StackLayout() { BackgroundColor = Color.Yellow };

				//set bindings
				nameLabel.SetBinding(Label.TextProperty, new Binding("Name"));
				typeLabel.SetBinding(Label.TextProperty, new Binding("Type"));
				image.SetBinding(Image.SourceProperty, new Binding("Image"));

				//Set properties for desired design
				horizontalLayout.Orientation = StackOrientation.Horizontal;
				horizontalLayout.HorizontalOptions = LayoutOptions.Fill;
				image.HorizontalOptions = LayoutOptions.End;
				nameLabel.FontSize = 24;

				//add views to the view hierarchy
				verticaLayout.Children.Add(nameLabel);
				verticaLayout.Children.Add(typeLabel);
				horizontalLayout.Children.Add(verticaLayout);
				horizontalLayout.Children.Add(image);
				
				// add to parent view
				View = horizontalLayout;
			}
		}
	}
}


